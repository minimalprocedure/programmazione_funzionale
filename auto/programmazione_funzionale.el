(TeX-add-style-hook
 "programmazione_funzionale"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "italian") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "babel"
    "fontenc"
    "fontspec"
    "graphicx"
    "mathtools"
    "algpseudocode"
    "algorithm"
    "listings"
    "minted"
    "hyperref"
    "ulem"
    "tikz"
    "lastpage"
    "fancyhdr"
    "geometry")
   (TeX-add-symbols
    "dontdofcolorbox")
   (LaTeX-add-labels
    "sec:org029102a"
    "sec:org8d30c13"
    "sec:org75fb1d2"
    "sec:org206940a"
    "sec:org770db37"
    "org47ca899"
    "sec:org5948ee0"
    "sec:org17c57d8"
    "orgd7f1bc9"
    "orgca39d9c"
    "sec:orgadd4f0a"
    "sec:org3ff239d"
    "sec:org3000098"
    "sec:orgbd1bf40"
    "sec:org50777a4"
    "org6583792"
    "sec:orgbdc5483"
    "sec:org74cdea6"
    "org9568a3a"
    "sec:orgdd39727"
    "sec:orgc0c8d65"
    "sec:orge4b6c3c"
    "sec:org5ce8baf"
    "sec:orga5c9d57"
    "sec:org9b28caf"
    "org7ad2679"
    "sec:org6e4d4fe"
    "sec:org3b3297a"
    "sec:org13a82c7"
    "sec:org2803091"
    "sec:org37a2c5e"
    "sec:org91c9ba7"
    "sec:org5b5011a"
    "org93febc8"
    "sec:org18767fa"
    "sec:orge5e9b00"
    "sec:orgf427cf2"
    "orgcae1bc4"
    "sec:org4210547"
    "sec:orga012ed7"
    "sec:org3afaf77"
    "sec:org6131fd5"
    "sec:orgb519aef"
    "sec:org2257b3a"
    "sec:org995f512"
    "sec:orgf999641"
    "sec:org7b9630f"
    "sec:orgf704c5c"
    "sec:org598b9f2"
    "sec:org6c8bf76"))
 :latex)

